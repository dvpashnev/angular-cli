import { NgModule } from '@angular/core';
import { CommonModule  } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from 'src/app/features/components/products/products.component';
import { CartComponent } from 'src/app/features/components/cart/cart.component';
import { DashboardComponent } from 'src/app/features/components/admin/dashboard/dashboard.component';
import { NotFoundComponent } from 'src/app/features/components/not-found/not-found.component';
import { AuthGuard } from 'src/app/features/components/auth/auth.guard';

const routes: Routes = [
  { path: 'products', 
  loadChildren: () => import('src/app/features/components/products/products.module')
.then(m => m.ProductsModule)},
  { path: 'product/:id', 
  loadChildren: () => import('src/app/features/components/products/product/product.module')
.then(m => m.ProductModule)},
  { path: 'users', 
  loadChildren: () => import('src/app/features/components/users/users.module')
.then(m => m.UsersModule)},
  { path: 'user/:id', 
  loadChildren: () => import('src/app/features/components/user/user.module')
.then(m => m.UserModule)},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'cart', component: CartComponent},
  {
    path: 'admin',
    loadChildren: () => import('../../features/components/admin/admin.module').then(m => m.AdminModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('../../features/components/auth/auth.module').then(m => m.AuthModule)
  },
  { path: '', redirectTo: 'products', pathMatch: 'full'},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
