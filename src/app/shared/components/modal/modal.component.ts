import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Output() close = new EventEmitter();

  @ViewChild('overlay') overlay?: ElementRef<HTMLDivElement>;

  constructor() { }

  ngOnInit(): void {
  }

  closeModal() {
    this.close.emit();
  }
}
