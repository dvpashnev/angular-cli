import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { ModalOutletComponent } from './modal-outlet/modal-outlet.component';
import { PortalModule } from 'src/app/directives/portal.module';



@NgModule({
  declarations: [ModalComponent, ModalOutletComponent],
  imports: [
    CommonModule,
    PortalModule
  ],
  exports: [ModalComponent, ModalOutletComponent]
})
export class ModalModule { }
