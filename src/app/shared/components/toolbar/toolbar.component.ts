import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/features/components/admin/services/dashboard.service';
import { Option } from 'src/app/types/Option';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input() instanceId: string = '';
  @Input() title: string = '';
  @Input() name: string = '';
  @Input() filterOptions: string = '';
  @Input() sortOptions: string = '';
  @Input() deleteOption: string = '0';
  @Input() filterModel: string = '';
  @Input() filterField: string = '';
  @Input() filterString: string = '';
  @Input() sort: string = 'id';
  @Input() order: string = 'asc';
  @Output() newFilter = new EventEmitter<string>();
  @Output() newFilterString = new EventEmitter<string>();
  @Output() newSort = new EventEmitter<string>();
  @Output() newOrder = new EventEmitter<string>();
  parsedFilterOptions: Option[] = [];
  parsedSortOptions: Option[] = [];
  deleteOptionBoolean: boolean = false;
  showFilterModal = false;
  filterStringModel: { newFilterString: string } = {
    newFilterString: ''
  };

  constructor(
    private dashboardService: DashboardService,
    @Inject(DOCUMENT) private document: Document,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.parsedFilterOptions = JSON.parse(this.filterOptions) as Option[];
    this.parsedSortOptions = JSON.parse(this.sortOptions) as Option[];
    this.deleteOptionBoolean = this.deleteOption === '1';
  }

  openFilterModal() {
    this.showFilterModal = true;
  }

  closeFilterModal() {
    this.showFilterModal = false;
    const filterSelect = this.document.querySelector('select[name="filter"]') as HTMLSelectElement;
    filterSelect.value = '';
  }

  onFilterChange(event: Event) {
    const element = event.target as HTMLSelectElement;
    if (element.value !== '') {
      const filterOptions = element.value.split(' ');
      this.filterModel = filterOptions[0];
      this.filterField = filterOptions[1];
      this.showFilterModal = true;
    } else {
      this.filterModel = '';
      this.filterField = '';
    }
    this.newFilter.emit(element.value);
  }

  onFilterSubmit(){
    this.filterString = this.filterStringModel.newFilterString;
    this.filterStringModel = {
      newFilterString: ''
    };
    this.newFilterString.emit(this.filterString);
    this.showFilterModal = false;
  }

  onSortChange(event: Event) {
    const element = event.target as HTMLSelectElement;
    this.sort = element.value;
    this.newSort.emit(this.sort);
  }

  onOrderChange(event: Event) {
    const element = event.target as HTMLButtonElement;
    this.order = element.innerText.toLowerCase();
    this.newOrder.emit(this.order);
  }

  onBoardDelete() {
    if(confirm('Are you sure?')){
        this.dashboardService.deleteBoard(this.instanceId as string).subscribe(console.log);
        this.router.navigateByUrl('/admin');
    }
  }
}
