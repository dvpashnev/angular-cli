import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar.component';
import { ButtonModule } from '../button/button.module';
import { PortalModule } from 'src/app/directives/portal.module';
import { ModalModule } from '../modal/modal.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ToolbarComponent
  ],
  imports: [
    CommonModule, ButtonModule, PortalModule, ModalModule, FormsModule
  ],
  exports: [
    ToolbarComponent
  ]
})
export class ToolbarModule { }
