import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: [
    'input { border: 1px solid green; }'
  ]
})
export class InputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
