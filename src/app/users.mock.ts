import { User } from './user';

export const USERS: User[] = [
  {
    id: 'user-1',
    name: 'John Dou',
    email: 'john.dou@test.com',
    age: 23
  },
  {
    id: 'user-2',
    name: 'Tom Smith',
    email: 'tom.smith@test.com',
    age: 32
  },
  {
    id: 'user-3',
    name: 'Karen Thomson',
    email: 'karen@test.com',
    age: 19
  },
  {
    id: 'user-4',
    name: 'Mary Smith',
    email: 'smith123@test.com',
    age: 30
  },
]
