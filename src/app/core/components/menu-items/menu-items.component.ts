import { Component, OnInit, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { MenuItem } from '../../models';
import { selectMenuItems } from '../../store/menus/menus.selector';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { addMenuItemFormSubmitted } from '../../store/menus/menus.actions';
import { Observable } from 'rxjs';

export interface DialogData {
  id: string;
  route: string;
}

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.scss']
})
export class MenuItemsComponent implements OnInit {
  menuItems: MenuItem[] = [];
  menuItems$: Observable<MenuItem[]>;

  constructor(
    private store: Store,
    public dialog: MatDialog
  ) { 
    this.menuItems$ = store.select(selectMenuItems);
  }

  id: number = 0;
  text: string = '';
  route: string = '';

  ngOnInit(): void {
    this.menuItems$.subscribe((menuItems) => this.menuItems = menuItems);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddItemDialog, {
      width: '250px',
      data: {id: this.id, route: this.route},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.text = result.text;
        this.route = result.route;
        this.store.dispatch(addMenuItemFormSubmitted({
          menuItem: {id: this.id, text: this.text, route: this.route},
        }));
      }
    });
  }
}

@Component({
  selector: 'add-item-dialog',
  templateUrl: 'add-item-dialog.html',
})
export class AddItemDialog {
  constructor(
    public dialogRef: MatDialogRef<AddItemDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}
}
