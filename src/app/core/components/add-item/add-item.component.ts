import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

export interface DialogData {
  text: string;
  route: string;
}

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {
  //menuItem = MenuItemPlaceholder;
  data: DialogData = {
    text: '',
    route: ''
  }
  constructor(
    // private location: Location,
    private store: Store
  ) { }

  ngOnInit(): void {
  }

  /* submit(menu: BaseMenuItem): void {
    this.store.dispatch(addMenuItemFormSubmitted({
      menuItem: menu,
    }));
  } */

}
