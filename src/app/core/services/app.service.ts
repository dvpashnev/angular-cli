import { Injectable } from '@angular/core';
import { USERS } from '../../users.mock';
import { map, Observable, of } from 'rxjs';
import { User } from '../../user';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor() {
  }

  getUsers(): Observable<User[]> {
    return of(USERS);
  }

  getUser(id: string): Observable<User> {
    return of(USERS).pipe(
      map(users => users.find(user => user.id === id)!)
    )
  }
}
