import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { MenuItem } from '../models';
import { MenusState } from '../store/menus';
import { selectMenuItems, selectMenus } from '../store/menus/menus.selector';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  menuItems: MenuItem[] = [];
  //menus$: Observable<MenusState>;

  constructor(private store: Store<{ menus: MenusState }>) {
    //this.menus$ = store.select('menus');
    //console.log(this.menus$);

    const menuItemsInStorage = localStorage.getItem('menus');
    if (!menuItemsInStorage) {
      localStorage.setItem('menus', JSON.stringify(this.menuItems));
    } else {
      this.menuItems = JSON.parse(menuItemsInStorage);
    }
  }

  getItems() {
    return of(this.menuItems);// new Observable((observer) => observer.next(this.menuItems));
    //return selectMenuItems;// new Observable((observer) => observer.next(this.menuItems));
  }

  addItem(menuItem: MenuItem) {
    //console.log('this.menuItems.length = ', this.menuItems.length);
    const newMenuItem = {...menuItem, id: this.menuItems.length };
    this.menuItems = [...this.menuItems];
    this.menuItems[newMenuItem.id] = newMenuItem;
        //console.log('newMenuItem', newMenuItem);
    localStorage.setItem('menus', JSON.stringify(this.menuItems));
    return of(this.menuItems);
  }

  updateItem(menuItem: MenuItem) {
    const index = this.menuItems.findIndex((item) => item.id === menuItem.id);
    this.menuItems = [...this.menuItems];
    this.menuItems[index] = menuItem;
    localStorage.setItem('menus', JSON.stringify(this.menuItems));
    return of(this.menuItems);
  }
}

