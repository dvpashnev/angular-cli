import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { Product } from 'src/app/types/Product';
import { products } from '../../products.mock';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor() { }

  getProducts() {
    return of(products);
  }

  getProduct(id: string) : Observable<Product> {
    return of(products).pipe(
      map((products: Product[]) => products.find((product: Product) => product.id === id) as Product));
  }
}
