export default class BaseMenuItem {
    id: number = 0;
    text: string = '';
    route: string = '';
}