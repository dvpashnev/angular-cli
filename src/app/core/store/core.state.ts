import { MenusState } from './menus';

export interface State {
    menus: MenusState;
}