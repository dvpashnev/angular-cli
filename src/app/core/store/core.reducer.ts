import { ActionReducerMap, MetaReducer } from "@ngrx/store";
import { State } from "./core.state";
import * as MenusReducer from "./menus/menus.reducer"

export const reducers: ActionReducerMap<State> = {
    menus: MenusReducer.menuReducer
};

export const metaReducers: MetaReducer<State>[] = [];