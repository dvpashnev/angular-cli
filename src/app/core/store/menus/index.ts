import { MenusState } from '../menus/menus.state';
import { 
    appLoaded,
    fetchMenuSuccess, 
    fetchMenuFailed,
    addMenuItemFormSubmitted,
    addMenuItemSuccess,
    addMenuItemFailed,
    editMenuItemFormSubmitted,
    editMenuItemSuccess,
    editMenuItemFailed,
    deleteMenuItemInitiated 
} from './menus.actions';

const MenusActions = {
    appLoaded,
    fetchMenuSuccess,
    fetchMenuFailed,
    addMenuItemFormSubmitted,
    addMenuItemSuccess,
    addMenuItemFailed,
    editMenuItemFormSubmitted,
    editMenuItemSuccess,
    editMenuItemFailed,
    deleteMenuItemInitiated
}

export { MenusState, MenusActions };