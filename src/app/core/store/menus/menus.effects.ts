import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, of, switchMap, tap } from "rxjs";
import { ActionWithMenuItem } from "src/app/types/ActionWithMenuItem";
import { MenusActions } from ".";
import { MenuItem } from "../../models";
import { ApiService } from "../../services/api.service";

@Injectable()
export class MenusEffects {
    constructor(
        //private location: Location,
        private actions$: Actions<any>,
        private apiService: ApiService
    ) {}

    fetchMenus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(
                MenusActions.appLoaded.type, MenusActions.addMenuItemSuccess.type
            ),
            switchMap(() =>
                this.apiService.getItems().pipe(
                    map((menuItems: MenuItem[]) => {
                        return MenusActions.fetchMenuSuccess({ menuItems })
                    }
                    ),
                    catchError((error) => 
                        of(MenusActions.fetchMenuFailed({ error }))
                    )
                )
            )
        )
    );

    addMenus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MenusActions.addMenuItemFormSubmitted.type),
            switchMap((action: ActionWithMenuItem) =>
                this.apiService.addItem(action.menuItem).pipe(
                    //tap(() => this.router.navigate("/menu")),
                    map(() =>  MenusActions.addMenuItemSuccess({ menuItem: action.menuItem })),
                    catchError((error) => 
                        of(MenusActions.addMenuItemFailed({ error }))
                    )
                )
            )
        )
    );

    editMenus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MenusActions.editMenuItemFormSubmitted.type),
            switchMap((action: ActionWithMenuItem) =>
                this.apiService.updateItem(action.menuItem).pipe(
                    // tap(() => this.location.back()),
                    map(() =>
                        MenusActions.editMenuItemSuccess({ menuItem: action.menuItem })
                    ),
                    catchError((error) => 
                        of(MenusActions.editMenuItemFailed({ error }))
                    )
                )
            )
        )
    );
}