import { createReducer, on } from "@ngrx/store";
import { MenusActions } from ".";
import { initialState } from "./menus.state";

export const menuReducer = createReducer(
    initialState,
    on(MenusActions.appLoaded, (state) => {
        return state;
    }),
    on(MenusActions.fetchMenuSuccess, (state, { menuItems }) => {
        return {
            ...state,
            menuItems: menuItems
        }
    }),
    on(MenusActions.addMenuItemSuccess, (state, { menuItem }) => {
        //console.log('addMenuItemSuccess');
        //console.log('state', state);
        //console.log('menuItem', menuItem);
        const newMenuItem = {...menuItem, id: state.menuItems.length };
        const updatedMenuItems = [...state.menuItems];
        updatedMenuItems[newMenuItem.id] = newMenuItem;
        const newState = {
            ...state,
            menuItems: updatedMenuItems
        };
        //console.log('newState', newState);
        return newState;
    }),
    on(MenusActions.editMenuItemSuccess, (state, { menuItem }) => {
        //console.log('editMenuItemSuccess');
        const menuItemIndex = state.menuItems.findIndex(
            (item) => item.id === menuItem.id
        );
        const updatedMenuItems = [...state.menuItems];
        updatedMenuItems[menuItemIndex] = menuItem;
        return {
            ...state,
            menuItems: updatedMenuItems
        }
    }),
)