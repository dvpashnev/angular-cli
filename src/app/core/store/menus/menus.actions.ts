import { createAction, props } from "@ngrx/store";
import { BaseMenuItem, MenuItem } from "../../models";

export const appLoaded = createAction(
    "[App] App Loaded"
);

export const fetchMenuSuccess = createAction(
    "[Menu API] Fetch Menu Success",
    props<{ menuItems: MenuItem[] }>()
);

export const fetchMenuFailed = createAction(
    "[Menu API] Fetch Menu Failed",
    props<{ error: any }>()
);

export const addMenuItemFormSubmitted = createAction(
    "[Add menu Page] Add Menu Item Form Submitted",
    props<{ menuItem: BaseMenuItem }>()
);

export const addMenuItemSuccess = createAction(
    "[Add menu Page] Add Menu Item Success",
    props<{ menuItem: BaseMenuItem }>()
);

export const addMenuItemFailed = createAction(
    "[Add menu Page] Add Menu Item Failed",
    props<{ error: Error }>()
);

export const editMenuItemFormSubmitted = createAction(
    "[Edit menu Page] Edit Menu Item Form Submitted",
    props<{ menuItem: MenuItem }>()
);

export const editMenuItemSuccess = createAction(
    "[Edit menu Page] Edit Menu Item Success",
    props<{ menuItem: MenuItem }>()
);

export const editMenuItemFailed = createAction(
    "[Edit menu Page] Edit Menu Item Failed",
    props<{ error: Error }>()
);

export const deleteMenuItemInitiated = createAction(
    "[Delete menu Page] Delete Menu Item Form Initiated",
    props<{ menuId: string }>()
);