import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './services/authentication.service';
import { AddItemComponent } from './components/add-item/add-item.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { AddItemDialog, MenuItemsComponent } from './components/menu-items/menu-items.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RoutingModule } from '../routing/routing/routing.module';

@NgModule({
  declarations: [
    HeaderComponent, FooterComponent, AddItemComponent, AddItemDialog, MenuItemsComponent
  ],
  imports: [
    CommonModule, MatDialogModule, MatFormFieldModule, MatInputModule, FormsModule, 
    MatCardModule, MatDividerModule, MatToolbarModule, MatIconModule, MatMenuModule, 
    MatMenuModule, RoutingModule
  ],
  exports:[
    HeaderComponent, FooterComponent, AddItemComponent, AddItemDialog, MenuItemsComponent
  ],
  providers: [
    AuthenticationService
  ]
})
export class CoreModule { }
