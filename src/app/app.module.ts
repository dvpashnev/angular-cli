import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CartModule } from './features/components/cart/cart.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from './core/store/core.reducer';
import { MenusEffects } from './core/store/menus/menus.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { RoutingModule } from './routing/routing/routing.module';
import { UsersModule } from './features/components/users/users.module';
import { PortalModule } from './directives/portal.module';
import { ModalModule } from './shared/components/modal/modal.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, CoreModule, CartModule, RoutingModule, UsersModule, PortalModule, ModalModule,
    BrowserAnimationsModule, StoreModule.forRoot(reducers, {}), EffectsModule.forRoot([MenusEffects]), 
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
