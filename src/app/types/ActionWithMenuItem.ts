import { MenuItem } from "../core/models";

export interface ActionWithMenuItem {
    menuItem: MenuItem;
}