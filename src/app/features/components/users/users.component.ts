import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../../../user';
import { AppService } from '../../../core/services/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]> = of([])

  constructor(
    private appService: AppService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.users$ = this.appService.getUsers();
  }

  goToUser(id: string) {
    this.router.navigate(['/user', id, { boo: 'boo' }]);
  }
}
