import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../../../core/services/app.service';
import { EMPTY, Observable, switchMap } from 'rxjs';
import { User } from '../../../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user$: Observable<User> = EMPTY;

  constructor(
    private route: ActivatedRoute,
    private appService: AppService
  ) {
  }

  ngOnInit(): void {
    this.user$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.appService.getUser(params.get('id')!))
      );
  }

}
