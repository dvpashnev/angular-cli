import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductModule } from './product/product.module';
import { ProductsComponent } from './products.component';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { InputModule } from 'src/app/shared/components/input/input.module';
import { ProductsRoutingModule } from './products-routing.module';

@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule, ButtonModule, InputModule, ProductModule, ProductsRoutingModule
  ],
  exports:[
    ProductsComponent
  ]
})
export class ProductsModule { }
