import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';

import {MatCardModule} from '@angular/material/card';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { ProductRoutingModule } from './product-routing.module';


@NgModule({
  declarations: [
    ProductComponent
  ],
  imports: [
    CommonModule, ButtonModule, MatCardModule, ProductRoutingModule
  ],
  exports:[
    ProductComponent
  ]
})
export class ProductModule { }
