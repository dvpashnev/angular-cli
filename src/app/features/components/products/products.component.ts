import { Component, OnInit } from '@angular/core';
import { EMPTY, mergeMap, Observable, toArray } from 'rxjs';
import { ProductsService } from 'src/app/core/services/products.service';
import { Product } from 'src/app/types/Product';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  //products: Product[] = [];
  products$: Observable<Product[]> = EMPTY;

  constructor(
    private productsService: ProductsService
  ) { }

  ngOnInit(): void {
    this.products$ = this.productsService.getProducts();
    //this.products$.subscribe(products => this.products = products);
  }

}
