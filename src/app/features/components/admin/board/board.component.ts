import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { EMPTY, fromEvent, map, Observable, of, Subscription, switchMap, takeUntil } from 'rxjs';
import { Status } from 'src/app/types/Status';
import { Option } from 'src/app/types/Option';
import Board from '../models/Board';
import Task from '../models/Task';
import { DashboardService } from '../services/dashboard.service';
import { BoardService } from '../services/board.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  board$: Observable<Board> = EMPTY;
  newTasks$: Observable<Task[]> = of([]);
  progressTasks$: Observable<Task[]> = of([]);
  doneTasks$: Observable<Task[]> = of([]);
  boardId: string = '';
  statuses: string[] = [
    Status.ToDo as string,
    Status.InProgress as string,
    Status.Done as string
  ];
  filterOptions: Option[] = [
    {
      value: 'Task name', content: 'Task Name'
    },
  ];
  sortOptions: Option[] = [
    {
      value: 'name', content: 'Name',
    },
    {
      value: 'date', content: 'Date',
    },
  ];
  deleteOption: string = '1';
  stringifiedFilterOptions: string = '';
  stringifiedSortOptions: string = '';
  @Input() filterModel: string = '';
  @Input() filterField: string = '';
  @Input() filterString: string = '';
  @Input() sort: string = 'id';
  @Input() order: string = 'asc';
  @Output() newColumnFilter = new EventEmitter<string>();
  @Output() newColumnFilterString = new EventEmitter<string>();
  @Output() newColumnSort = new EventEmitter<string>();
  @Output() newColumnOrder = new EventEmitter<string>();

  mousemove$: Observable<Event> = EMPTY;
  mouseup$: Observable<Event> = EMPTY;
  mousemoveSubscription: Subscription = new Subscription();
  mouseupSubscription: Subscription = new Subscription();
  originalColumn: HTMLDivElement | null = null;
  draggable: HTMLDivElement | null = null;

  constructor(
    private route: ActivatedRoute,
    private dashboardService: DashboardService,
    private boardService: BoardService,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => this.boardId = params.get('id')!);
    this.board$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.dashboardService.getBoard(params.get('id')!))
      );
    this.setTasks();
    this.stringifiedFilterOptions = JSON.stringify(this.filterOptions);
    this.stringifiedSortOptions = JSON.stringify(this.sortOptions);
  }

  setTasks() {
    this.newTasks$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.boardService.getTasksByBoardAndType(params.get('id')!, Status.ToDo))
      );
    this.progressTasks$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.boardService.getTasksByBoardAndType(params.get('id')!, Status.InProgress))
      );
    this.doneTasks$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.boardService.getTasksByBoardAndType(params.get('id')!, Status.Done))
      );
  }

  setFilter(filter: string) {
    if (filter !== '') {
      const filterOptions = filter.split(' ');
      this.filterModel = filterOptions[0];
      this.filterField = filterOptions[1];
    } else {
      this.filterModel = '';
      this.filterField = '';
      this.filterString = '';
    }
    this.boardService.setFilterModel(this.filterModel);
    this.boardService.setFilterField(this.filterField);
  }

  setFilterString(filterString: string) {
    this.filterString = filterString;
    this.boardService.setFilterString(this.filterString);
    this.board$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.dashboardService.getBoard(params.get('id')!))
      );
  }

  setSort(sort: string) {
    this.sort = sort;
    this.boardService.setSort(sort);
    this.board$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.dashboardService.getBoard(params.get('id')!))
      );
  }

  setOrder(order: string) {
    this.order = order;
    this.boardService.setOrder(order);
    this.board$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.dashboardService.getBoard(params.get('id')!))
      );
  }

  onDragStart() {
    return false;
  }

  onMouseDown(event: MouseEvent) {
    const target = event.target as HTMLElement;    
    this.draggable = target.closest('.draggable') as HTMLDivElement;
    if (this.draggable) {
      this.originalColumn = this.draggable.closest('.tasks') as HTMLDivElement;
      const shiftX = (event as MouseEvent).clientX - this.draggable.getBoundingClientRect().left;
      const shiftY = (event as MouseEvent).clientY - this.draggable.getBoundingClientRect().top;
      const width = this.draggable.getBoundingClientRect().width;
      this.draggable.style.left =
              (event as MouseEvent).clientX - shiftX + 'px';
            this.draggable.style.top =
              (event as MouseEvent).clientY - shiftY + 'px';
      this.draggable.style.position = 'absolute';
      this.draggable.style.zIndex = '1000';
      this.draggable.style.width = width + 'px';
      this.draggable.id = 'dragged';
      this.document.body.append(this.draggable);
      this.mousemove$ = fromEvent(window, 'mousemove').pipe(
        map((event: Event) => {
          if (this.draggable) {
            this.draggable.style.left =
              (event as MouseEvent).pageX - shiftX + 'px';
            this.draggable.style.top =
              (event as MouseEvent).pageY - shiftY + 'px';
          }
          return event;
        }),
        takeUntil(this.mouseup$)
      );
      this.mouseup$ = fromEvent(window, 'mouseup').pipe(
        map((event: Event) => {
          if (!this.draggable) {
            this.draggable = this.document.querySelector('#dragged');
          }
          if (this.draggable) {
            this.draggable.style.display = 'none';
            let elemBelow = document.elementFromPoint((event as MouseEvent).clientX, (event as MouseEvent).clientY);
            this.draggable.style.display = 'flex';
            console.log(elemBelow);
            if (elemBelow) {
              this.draggable.style.left = '0';
              this.draggable.style.top = '0';
              this.draggable.style.position = 'relative';
              this.draggable.style.zIndex = '1';
              this.draggable.style.width = '100%';
              this.draggable.removeAttribute('id');
              const tasksBlock = elemBelow.closest('.tasks') as HTMLDivElement;
              console.log(tasksBlock);
              if (tasksBlock) {
                tasksBlock.append(this.draggable);
                const taskId = this.draggable.getAttribute('data-id');
                console.log(taskId);
                const column = tasksBlock.closest('.column-content') as HTMLDivElement;
                const status = column.getAttribute('data-type');
                console.log(status);
                this.boardService.getTask(taskId as string).subscribe(
                  (task) => {
                    task.status = status as Status;
                    this.boardService.patchTask(task).subscribe(console.log);
                  }
                );
              } else {
                this.originalColumn?.append(this.draggable);
              }
              this.mousemoveSubscription.unsubscribe();
              this.mouseupSubscription.unsubscribe();
              this.draggable = null;
            }
          }
          return event;
        })
      );
      this.mousemoveSubscription = this.mousemove$.subscribe(console.log);
      this.mouseupSubscription = this.mouseup$.subscribe(console.log);
    }
  }
}
