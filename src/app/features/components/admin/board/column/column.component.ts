import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { EMPTY, Observable, of, switchMap } from 'rxjs';
import { Status } from 'src/app/types/Status';
import Board from '../../models/Board';
import Task from '../../models/Task';
import { BoardService } from '../../services/board.service';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {
  @Input() type: string = Status.ToDo;
  @Input() boardId: string = '';
  title: string = '';
  board$: Observable<Board> = EMPTY;
  tasks$: Observable<Task[]> = of([]);
  showTaskModal: boolean = false;
  newTask: Task = {
    id: '',
    boardId: '',
    name: '',
    description: '',
    status: this.type as Status,
    dateOfCreation: new Date()
  }
  @Input() filterModel: string = '';
  @Input() filterField: string = '';
  @Input() filterString: string = '';
  @Input() sort: string = 'id';
  @Input() order: string = 'asc';

  constructor(
    private route: ActivatedRoute,
    private dashboardService: DashboardService,
    private boardService: BoardService,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {
    this.title = this.type.split(/(?=[A-Z])/).join(' ');
    this.route.paramMap.subscribe((params: ParamMap) => this.boardId = params.get('id')!);
    this.board$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.dashboardService.getBoard(params.get('id')!))
      );
    this.tasks$ = this.route.paramMap
      .pipe(
        switchMap((params: ParamMap) =>
          this.boardService.getTasksByBoardAndType(params.get('id')!, this.type))
      );
  }

  openTaskModal() {
    this.showTaskModal = true;
  }

  closeTaskModal() {
    this.showTaskModal = false;
  }

  onSubmit(){
    this.newTask.boardId = this.boardId;
    this.newTask.status = this.type as Status;
    if (this.newTask.id === '') {
      this.boardService.postTask(this.newTask);
    } else {
      this.boardService.patchTask(this.newTask).subscribe(console.log);
      this.newTask = {
        id: '',
        boardId: '',
        name: '',
        description: '',
        status: this.type as Status,
        dateOfCreation: new Date()
      };
    }
    this.showTaskModal = false;
    this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
  }

  onPopup(event: Event) {
    const target = event.target as HTMLDivElement;
    const optionsDiv = target.closest('.popup');
    if (optionsDiv) {
      const taskId = optionsDiv.getAttribute('id');
      const popup = document.getElementById(`popup${taskId}`);
      if (popup) {
        popup.classList.toggle("show");
      }
    }
  }

  onEdit(event: Event) {
    const target = event.target as HTMLDivElement;
    const optionsDiv = target.closest('.popup');
    if (optionsDiv) {
      this.showTaskModal = true;
      const taskId = optionsDiv.getAttribute('id');
      this.boardService.getTask(taskId as string).subscribe((task) => {
        this.newTask = task;
      });
    }
  }

  onDelete(event: Event) {
    if(confirm('Are you sure?')){
      const target = event.target as HTMLDivElement;
      const optionsDiv = target.closest('.popup');
      if (optionsDiv) {
        const taskId = optionsDiv.getAttribute('id');
        this.boardService.deleteTask(taskId as string, this.boardId, this.type);
        this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
      }
    }
  }

  onArchive(event: Event) {
    if(confirm('Are you sure?')){
      const target = event.target as HTMLDivElement;
      const optionsDiv = target.closest('.popup');
      if (optionsDiv) {
        const taskId = optionsDiv.getAttribute('id');
        this.boardService.getTask(taskId as string).subscribe((task) => {
          task.status = Status.Archive;
          this.boardService.patchTask(task).subscribe(console.log);
          this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
        });
      }
    }
  }

  setFilter(filter: string) {
    if (filter !== '') {
      const filterOptions = filter.split(' ');
      this.filterModel = filterOptions[0];
      this.filterField = filterOptions[1];
    } else {
      this.filterModel = '';
      this.filterField = '';
      this.filterString = '';
    }
    this.dashboardService.setFilterModel(this.filterModel);
    this.dashboardService.setFilterField(this.filterField);
    this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
  }

  setFilterString(filterString: string) {
    this.filterString = filterString;
    this.dashboardService.setFilterString(this.filterString);
    this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
  }

  setSort(sort: string) {
    console.log('setSort: ', sort);
    this.sort = sort;
    this.boardService.setSort(sort);
    this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
  }

  setOrder(order: string) {
    console.log('setOrder: ', order);
    this.order = order;
    this.boardService.setOrder(order);
    this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
  }
}
