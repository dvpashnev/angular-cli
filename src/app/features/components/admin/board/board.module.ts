import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TaskComponent } from './task/task.component';
import { ColumnComponent } from './column/column.component';
import { BoardComponent } from './board.component';
import { ToolbarModule } from 'src/app/shared/components/toolbar/toolbar.module';
import { BoardRoutingModule } from './board-routing.module';
import { ModalModule } from 'src/app/shared/components/modal/modal.module';
import { PortalModule } from 'src/app/directives/portal.module';



@NgModule({
  declarations: [
    BoardComponent,
    TaskComponent,
    ColumnComponent
  ],
  imports: [
    CommonModule, FormsModule, DatePipe, ToolbarModule, BoardRoutingModule, ModalModule, PortalModule 
  ],
  exports: [
    BoardComponent
  ]
})
export class BoardModule { }
