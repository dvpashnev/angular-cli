import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  @Input() taskId: string = '';
  constructor() { }

  ngOnInit(): void {
  }

  /* onPopup(event: Event) {
    const target = event.target as HTMLDivElement;
    const optionsDiv = target.closest('.popup');
    if (optionsDiv) {
      const taskId = optionsDiv.getAttribute('id');
      const popup = document.getElementById(`popup${taskId}`);
      if (popup) {
        popup.classList.toggle("show");
      }
    }
  }

  onEdit(event: Event) {
    const target = event.target as HTMLDivElement;
    const optionsDiv = target.closest('.popup');
    if (optionsDiv) {
      this.showTaskModal = true;
      const taskId = optionsDiv.getAttribute('id');
      this.boardService.getTask(taskId as string).subscribe((task) => {
        this.newTask = task;
      });
    }
  }

  onDelete(event: Event) {
    if(confirm('Are you sure?')){
      const target = event.target as HTMLDivElement;
      const optionsDiv = target.closest('.popup');
      if (optionsDiv) {
        const taskId = optionsDiv.getAttribute('id');
        this.boardService.deleteTask(taskId as string, this.boardId, this.type);
        this.tasks$ = this.boardService.getTasksByBoardAndType(this.boardId, this.type);
      }
    }
  } */

}
