export default class Board {
    [index: string]: string | Date | number;
    id: string = '';
    name: string = '';
    description: string = '';
    dateOfCreation: Date = new Date();
    new: number = 0;
    progress: number = 0;
    done: number = 0;

}