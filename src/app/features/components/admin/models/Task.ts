import { Status } from "src/app/types/Status";

export default class Task {
    [index: string]: string | Date | Status;
    id: string = '';
    boardId: string = '';
    name: string = '';
    description: string = '';
    dateOfCreation: Date = new Date();
    status: Status = Status.ToDo;
}