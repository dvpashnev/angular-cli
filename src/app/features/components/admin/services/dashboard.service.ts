import { Injectable } from '@angular/core';
import { filter, from, map, mergeMap, Observable, switchMap, concatMap, reduce, EMPTY } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import Board from 'src/app/features/components/admin/models/Board';
import Task from 'src/app/features/components/admin/models/Task';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiUrl: string = 'http://localhost:3000/';
  private filterModel: string | null = null;
  private filterField: string = '';
  private filterString: string | null = null;
  private sort: string = 'id';
  private order: string = 'asc';
  
  constructor() { }

  getBoards(): Observable<Board[]> {
    return ajax({
      method: 'GET',
      url: `${this.apiUrl}boards?_sort=${this.sort}&_order=${this.order}`,
      responseType: 'json'
    }).pipe(map(res => res.response as Board[]));
  }

  getBoard(id: string): Observable<Board> {
    return ajax(`${this.apiUrl}boards/${id}`).pipe(
      map(res => res.response as Board)
    );
  }

  postBoard(board: Board): Observable<unknown> {
    return ajax({
      method: 'POST',
      url: `${this.apiUrl}boards`,
      body: board
    }).pipe(map(res => res.response));
  }

  patchBoard(board: Board) {
    return ajax({
      method: 'PATCH',
      url: `${this.apiUrl}boards/${board.id}`,
      body: board
    });
  }

  deleteBoard(id: string) {
    return ajax({
      method: 'DELETE',
      url: `${this.apiUrl}boards/${id}`
    });
  }

  filterBoards(): Observable<Board[]> {
    if (this.filterModel === 'Task') {
      const boards: Board[] = [];
      return ajax({
          method: 'GET',
          url: `${this.apiUrl}tasks`,
          responseType: 'json'
        }).pipe(
            map((res) => res.response as Task[]),
            map((tasks: Task[]) => {
              return tasks.filter(task => (task[this.filterField] as string).indexOf(this.filterString as string) >= 0) 
            }),
            mergeMap((tasks) => {
              return from(tasks);
            }),
            mergeMap((task: Task) => {
              return ajax({
                method: 'GET',
                url: `${this.apiUrl}boards?id=${task.boardId}&_sort=${this.sort}&_order=${this.order}`,
                responseType: 'json'
              })}),
            mergeMap((res) => {
              return res.response as Board[];
            }),
            reduce((acc: Board[], board: Board) => {
              console.log('acc', acc);
              console.log('indexOf board', acc.find(item => item.id === board.id));
              if (!acc.find(item => item.id === board.id)) {
                acc.push(board);
              }
              return acc;
            }, [])
          );
      }
      return ajax({
          method: 'GET',
          url: `${this.apiUrl}boards?_sort=${this.sort}&_order=${this.order}`,
          responseType: 'json'
        }).pipe(
          map((res) => res.response as Board[]),
          map((boards: Board[]) => {
            return boards.filter(board => (board[this.filterField] as string).indexOf(this.filterString as string) >= 0) 
          })
        );
  }

  setFilterModel(filterModel: string) {
    this.filterModel = filterModel;
  }

  setFilterField(filterField: string) {
    this.filterField = filterField;
  }

  setFilterString(filterString: string) {
    this.filterString = filterString;
  }

  setSort(sort: string) {
    this.sort = sort;
  }

  setOrder(order: string) {
    this.order = order;
  }
}

