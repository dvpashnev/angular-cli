import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import Task from 'src/app/features/components/admin/models/Task';
import { Status } from 'src/app/types/Status';
import { DashboardService } from './dashboard.service';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  apiUrl: string = 'http://localhost:3000/';
  private filterModel: string = '';
  private filterField: string = '';
  private filterString: string = '';
  private sort: string = 'id';
  private order: string = 'asc';
  statuses: string[] = [
    Status.ToDo as string,
    Status.InProgress as string,
    Status.Done as string
  ];

  constructor(
    private dashboardService: DashboardService
  ) { }

  getStatuses(): Observable<string[]> {
    return of(this.statuses);
  }

  getTasks(): Observable<Task[]> {
    if (this.filterString === '') {
      return ajax({
        method: 'GET',
        url: `${this.apiUrl}tasks?_sort=${this.sort}&_order=${this.order}`,
        responseType: 'json'
      }).pipe(
          map((res) => res.response as Task[])
        );
    } else {
      return ajax({
          method: 'GET',
          url: `${this.apiUrl}tasks?_sort=${this.sort}&_order=${this.order}`,
          responseType: 'json'
        }).pipe(
          map((res) => res.response as Task[]),
          map((tasks: Task[]) => {
            return tasks.filter(task => (task[this.filterField] as string).indexOf(this.filterString as string) >= 0) 
          })
        );
    }
  }

  getBoardTasks(boardId: string): Observable<Task[]> {
    return ajax({
      method: 'GET',
      url: `${this.apiUrl}tasks?boardId=${boardId}&_sort=${this.sort}&_order=${this.order}`,
      responseType: 'json'
    }).pipe(
        map((res) => res.response as Task[])
      );
  }

  getTask(id: string): Observable<Task> {
    return ajax(`${this.apiUrl}tasks/${id}`).pipe(map(res => res.response as Task));
  }

  postTask(task: Task) {
    return ajax({
      method: 'POST',
      url: `${this.apiUrl}tasks`,
      body: task
    }).subscribe(() => {
      this.dashboardService.getBoard(task.boardId).subscribe((board) => {
        if (task.status === Status.ToDo){
          board.new++;
        } else if (task.status === Status.InProgress) {
          board.progress++;
        } else {
          board.done++;
        }
        this.dashboardService.patchBoard(board).subscribe();
      });
    });
  }

  patchTask(task: Task) {
    return ajax({
      method: 'PATCH',
      url: `${this.apiUrl}tasks/${task.id}`,
      body: task
    });
  }

  deleteTask(id: string, boardId: string, status: string) {
    return ajax({
      method: 'DELETE',
      url: `${this.apiUrl}tasks/${id}`
    }).subscribe(() => {
      this.dashboardService.getBoard(boardId).subscribe((board) => {
        if (status === Status.ToDo){
          board.new--;
        } else if (status === Status.InProgress) {
          board.progress--;
        } else {
          board.done--;
        }
        this.dashboardService.patchBoard(board).subscribe();
      });
    });
  }

  getTasksByName(name: string): Observable<Task[]> {
    return ajax({
      method: 'GET',
      url: `${this.apiUrl}tasks?name=${name}&_sort=${this.sort}&_order=${this.order}`,
      responseType: 'json'
    }).pipe(
        map((res) => res.response as Task[])
      );
  }

  getTasksByBoardAndType(boardId: string, type: string): Observable<Task[]> {
    if (this.filterString === '') {
      return ajax({
        method: 'GET',
        url: `${this.apiUrl}tasks?boardId=${boardId}&status=${type}&_sort=${this.sort}&_order=${this.order}`,
        responseType: 'json'
      }).pipe(
          map((res) => res.response as Task[])
        );
    } else {
      return ajax({
          method: 'GET',
          url: `${this.apiUrl}tasks?boardId=${boardId}&status=${type}&_sort=${this.sort}&_order=${this.order}`,
          responseType: 'json'
        }).pipe(
          map((res) => res.response as Task[]),
          map((tasks: Task[]) => {
            return tasks.filter(task => (task[this.filterField] as string).indexOf(this.filterString as string) >= 0) 
          })
        );
    }
  }

  setSort(sort: string) {
    this.sort = sort;
  }

  setOrder(order: string) {
    this.order = order;
  }

  setFilterModel(filterModel: string) {
    this.filterModel = filterModel;
  }

  setFilterField(filterField: string) {
    this.filterField = filterField;
  }

  setFilterString(filterString: string) {
    this.filterString = filterString;
  }

  filterTasks(): Observable<Task[]> {
      return ajax({
          method: 'GET',
          url: `${this.apiUrl}tasks?_sort=${this.sort}&_order=${this.order}`,
          responseType: 'json'
        }).pipe(
          map((res) => res.response as Task[]),
          map((tasks: Task[]) => {
            return tasks.filter(task => (task[this.filterField] as string).indexOf(this.filterString as string) >= 0) 
          })
        );
  }
}

