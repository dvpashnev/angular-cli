import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ToolbarModule } from 'src/app/shared/components/toolbar/toolbar.module';
import { BoardModule } from '../board/board.module';
import { FormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ModalModule } from 'src/app/shared/components/modal/modal.module';
import { PortalModule } from 'src/app/directives/portal.module';



@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule, ToolbarModule, BoardModule, FormsModule, DashboardRoutingModule, ModalModule, PortalModule
   ],
  exports:[
    DashboardComponent
  ]
})
export class DashboardModule { }
