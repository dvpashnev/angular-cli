import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Option } from 'src/app/types/Option';
import Board from '../models/Board';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  boards$: Observable<Board[]> = of([]);
  filterOptions: Option[] = [
    {
      value: 'Board name', content: 'Board Name'
    },
    {
      value: 'Task name', content: 'Task Name'
    },
  ];
  sortOptions: Option[] = [
    {
      value: 'name', content: 'Name',
    },
    {
      value: 'date', content: 'Date',
    },
    {
      value: 'new', content: 'Number of new task',
    },
    {
      value: 'progress', content: 'Number of in progress task',
    },
    {
      value: 'done', content: 'Number of done task',
    },
  ];
  stringifiedFilterOptions: string = '';
  stringifiedSortOptions: string = '';
  showBoardModal: boolean = false;
  newBoard: Board = {
    id: '',
    name: '',
    description: '',
    dateOfCreation: new Date(),
    new: 0,
    progress: 0,
    done: 0
  };
  filterModel: string = '';
  filterField: string = '';
  filterString: string = '';
  sort: string = 'id';
  order: string = 'asc';

  constructor(
    private dashboardService: DashboardService,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {
    this.boards$ = this.dashboardService.getBoards();
    this.stringifiedFilterOptions = JSON.stringify(this.filterOptions);
    this.stringifiedSortOptions = JSON.stringify(this.sortOptions);

    window.addEventListener('click', this.hideAdditionWindows, true);
  }

  hideAdditionWindows(event: Event) {
    const modal = this.document.querySelector(".modal") as HTMLElement;
    if (modal && event.target === modal) {
      modal.classList.add('hidden');
    }
    const openPopup = document.querySelector('.popuptext.show');
    if (openPopup) {
      openPopup.classList.remove("show");
    }
  }

  openBoardModal() {
    this.showBoardModal = true;
  }

  closeBoardModal() {
    this.showBoardModal = false;
  }

  onSubmit(){
    console.log(this.newBoard);
    if (this.newBoard.id === '') {
      this.dashboardService.postBoard(this.newBoard).subscribe(console.log);//
    } else {
      this.dashboardService.patchBoard(this.newBoard).subscribe(console.log);
      this.newBoard = {
        id: '',
        name: '',
        description: '',
        dateOfCreation: new Date(),
        new: 0,
        progress: 0,
        done: 0
      };
    }
    this.showBoardModal = false;
    this.boards$ = this.dashboardService.getBoards();
  }

  setFilter(filter: string) {
    if (filter !== '') {
      const filterOptions = filter.split(' ');
      this.filterModel = filterOptions[0];
      this.filterField = filterOptions[1];
    } else {
      this.filterModel = '';
      this.filterField = '';
      this.filterString = '';
    }
    this.dashboardService.setFilterModel(this.filterModel);
    this.dashboardService.setFilterField(this.filterField);
    this.boards$ = this.dashboardService.getBoards();
  }

  setFilterString(filterString: string) {
    this.filterString = filterString;
    this.dashboardService.setFilterString(this.filterString);
    this.boards$ = this.dashboardService.filterBoards();
  }

  setSort(sort: string) {
    this.sort = sort;
    this.dashboardService.setSort(sort);
    this.boards$ = this.dashboardService.getBoards();
  }

  setOrder(order: string) {
    this.order = order;
    this.dashboardService.setOrder(order);
    this.boards$ = this.dashboardService.getBoards();
  }

  onPopup(event: Event) {
    const target = event.target as HTMLDivElement;
    const optionsDiv = target.closest('.popup');
    if (optionsDiv) {
      const boardId = optionsDiv.getAttribute('id');
      const popup = document.getElementById(`boardPopup${boardId}`);
      if (popup) {
        popup.classList.toggle("show");
      }
    }
  }

  onEdit(event: Event) {
    const target = event.target as HTMLDivElement;
    const optionsDiv = target.closest('.popup');
    if (optionsDiv) {
      const taskId = optionsDiv.getAttribute('id');
      this.dashboardService.getBoard(taskId as string).subscribe((board) => {
        this.newBoard = board;
        this.showBoardModal = true;
      });
    }
  }

  onDelete(event: Event) {
    if(confirm('Are you sure?')){
      const target = event.target as HTMLDivElement;
      const optionsDiv = target.closest('.popup');
      if (optionsDiv) {
        const boardId = optionsDiv.getAttribute('id');
        console.log(this.dashboardService.deleteBoard(boardId as string).subscribe(console.log));
        this.boards$ = this.dashboardService.getBoards();
      }
    }
  }

  ngOnDestroy() {
    window.removeEventListener('click', this.hideAdditionWindows);
  }
}
