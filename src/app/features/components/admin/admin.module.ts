import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { TasksComponent } from './tasks/tasks.component';
import { RequestsComponent } from './requests/requests.component';
import { DashboardModule } from './dashboard/dashboard.module';


@NgModule({
  declarations: [
    AdminComponent,
    TasksComponent,
    RequestsComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    DashboardModule
  ]
})
export class AdminModule { }
